/*jslint node: true*/
'use strict';

var request = require('request');
var format  = require('util').format;

var URI_API        = 'http://api.blueori.com';
var APPNAME_API    =  "accounts-backend";
var APPVERSION_API = 'v1';
var APPNAME        = '';

function verify_auth(token, callback) {
    var options = {
        "json": true,
        "uri": format('%s/token/get?token=%s&app=%s', URI_API, token, APPNAME),
        "headers": {
            "appname": APPNAME_API,
            "appversion": APPVERSION_API
        }
    };

    request.get(options, function (err, result) {
        if (err || !result) {
            callback({
                "code": 1,
                "status": "ERROR",
                "result": "error on accounts server connection"
            });
            return;
        }

        if (result.body.code === 1) {
            callback(result.body);
            return;
        }

        if (result.body.code === 0) {
            callback(false, result.body.result);
            return;
        }

        callback({
            "code": 1,
            "status": "ERROR",
            "result": "error on accounts server content"
        });
    });
}

function middleware(req, res, next) {
    var token, msg_invalid;
    if (req.headers.account_token) {
        token = req.headers.account_token;
    } else if (req.query.account_token) {
        token = req.query.account_token;
    }

    if (!token) {
        msg_invalid = {
            "code": 1,
            "status": "ERROR",
            "result": "was not sent any token"
        };
        res.send(msg_invalid);
    }

    verify_auth(token, function (err, result) {
        if (err) {
            res.send(err);
            return;
        }

        if (!req.BLUEORI) {
            req.BLUEORI = {};
        }

        req.BLUEORI.ACCOUNT = result;

        next();
    });
}

function Accounts(appname) {
    if (!appname) {
        throw 'Need app name!';
    }

    APPNAME = appname;

    return {
        "verify_auth": verify_auth,
        "middleware": middleware
    };
}

module.exports = Accounts;
