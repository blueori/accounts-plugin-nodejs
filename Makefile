# Executables
JSLINT_EXEC = ./node_modules/jslint/bin/jslint.js

test: jslint

jslint:
	# Files
	$(JSLINT_EXEC) \
	'index.js' \
	'package.json' \

.PHONY: test jslint